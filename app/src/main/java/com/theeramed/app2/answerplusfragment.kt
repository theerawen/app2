package com.theeramed.app2


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.theeramed.app2.databinding.AnswerPlusBinding



class answerplusfragment : Fragment() {

    private var _binding: AnswerPlusBinding? = null
    private val binding get() = _binding!!
    private lateinit var answer: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        answer = arguments?.getString(ANSWER).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AnswerPlusBinding.inflate(inflater,container, false)
        binding.buttonRestartPlus.text = answer
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonRestartPlus.setOnClickListener {
            val action = answerplusfragmentDirections.actionAnswerPlusfragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    companion object {
        const val ANSWER = "answer"
    }
}