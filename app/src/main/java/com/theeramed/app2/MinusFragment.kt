package com.theeramed.app2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.theeramed.app2.databinding.MinusFragmentBinding



class MinusFragment : Fragment() {
    private var _binding: MinusFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = MinusFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.EqualMinus.setOnClickListener {
            var number1 = binding.textView1.text.toString().toInt()
            var number2 = binding.textView2.text.toString().toInt()
            var answerminus = number1 - number2
            val action =
                MinusFragmentDirections.actionMinusFragmentToAnswerFragment(answerminus = answerminus.toString())
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}