package com.theeramed.app2


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.theeramed.app2.databinding.AnswerFragmentBinding

class AnswerFragment : Fragment() {

    private var _binding: AnswerFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var answerminus: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        answerminus = arguments?.getString(ANSWERMINUS).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AnswerFragmentBinding.inflate(inflater, container, false)
        binding.textView5.text = answerminus
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnReminus.setOnClickListener {
            val action = AnswerFragmentDirections.actionAnswerFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {
        const val ANSWERMINUS = "answerminus"
    }
}
