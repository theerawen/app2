package com.theeramed.app2


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.theeramed.app2.databinding.PlusFragmentBinding



class PlusFragment : Fragment() {
    private var _binding: PlusFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = PlusFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textView4.setOnClickListener {
            var numberone = binding.textView.text.toString().toInt()
            var numbertwo = binding.textView2.text.toString().toInt()
            var answer = numberone + numbertwo
            val action =
                PlusFragmentDirections.actionPlusFragmentToAnswerPlusfragment(answer = answer.toString())
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}
